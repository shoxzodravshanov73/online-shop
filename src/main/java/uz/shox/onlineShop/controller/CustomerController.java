package uz.shox.onlineShop.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.shox.onlineShop.payload.ApiResponse;
import uz.shox.onlineShop.service.CustomerService;



@RestController
@RequestMapping("/api")
public class CustomerController {

    private final CustomerService customerService;


    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }


    //This method will bring in consumers who did not order in 2016
    @GetMapping("/getCustomersAllDetails")
    public HttpEntity<?> getCustomersAllDetails(){
        ApiResponse apiResponse=customerService.getCustomersAllDetails();
        return ResponseEntity.ok(apiResponse);
    }


    //This method provides the Buyer's ID and name and the last order date
    @GetMapping("/getCustomerLastOrderDate")
    public HttpEntity<?> getCustomerLastOrderDate(){
        ApiResponse apiResponse=customerService.getCustomerLastOrderDate();
        return ResponseEntity.ok(apiResponse);
    }
}
