package uz.shox.onlineShop.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.shox.onlineShop.payload.ApiResponse;
import uz.shox.onlineShop.service.InvoiceService;

import java.sql.Date;

@RestController
@RequestMapping("/api")
public class InvoiceController {

    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }


    /*
    this method returns all attributes of invoices issued after the specified period
     */
    @GetMapping("/getInvoice")
    public HttpEntity<?> getAfterDateDueInvoice(){
        ApiResponse apiResponse=invoiceService.getAfterDateDueInvoice();
        return  ResponseEntity.ok(apiResponse);
    }



    /*
    This method returns invoices issued before the date of the order.
 Returns the invoice ID,
 the date it was issued and the associated order ID and the date the order was placed
     */
    @GetMapping("/getInvoiceIdAndIssued")
    public HttpEntity<?> getBeforeDateOrderInvoice(){
        ApiResponse apiResponse=invoiceService.getBeforeDateOrderInvoice();
        return ResponseEntity.ok(apiResponse);
    }
}
