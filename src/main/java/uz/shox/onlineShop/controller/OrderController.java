package uz.shox.onlineShop.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.shox.onlineShop.payload.ApiResponse;
import uz.shox.onlineShop.payload.OrderDto;
import uz.shox.onlineShop.service.OrderService;


@RestController
@RequestMapping("/api")
public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    // This method we make order and create a new invoice
    @PostMapping("/orders")
    public HttpEntity<?> order(@RequestBody OrderDto orderDto){
        ApiResponse apiResponse=orderService.addOrder(orderDto);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }

    //This method get by order_id order details and product name;
    @GetMapping("/order/details")
    public HttpEntity<?> getOrderDetails(@RequestParam Integer orderId){
        ApiResponse apiResponse=orderService.getOrder(orderId);
        return ResponseEntity.ok(apiResponse);
    }

//This method gives orders placed before September 6, 2016
    @GetMapping("/getOrders")
    public HttpEntity<?> getOrders(){
        ApiResponse apiResponse=orderService.getOrders();
        return ResponseEntity.ok(apiResponse);
    }

    //This method gives you how many orders there were in 2016
    @GetMapping("/numberOfProductsInYear")
    public HttpEntity<?> numberOfProductsInYear(){
        ApiResponse apiResponse=orderService.numberOfProductsInYear();
        return ResponseEntity.ok(apiResponse);
    }


    //This method get orders without invoices
    @GetMapping("/getOrdersWithoutInvoices")
    public HttpEntity<?> getOrdersWithoutInvoices(){
        ApiResponse apiResponse=orderService.getOrdersWithoutInvoices();
        return ResponseEntity.ok(apiResponse);
    }
}
