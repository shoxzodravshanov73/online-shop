package uz.shox.onlineShop.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.shox.onlineShop.payload.ApiResponse;
import uz.shox.onlineShop.service.PaymentService;

@RestController
@RequestMapping("/api")
public class PaymentController {

   private final PaymentService paymentService;

    public PaymentController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    //This method to make payment
    @PostMapping("/payment")
    public HttpEntity<?> paymentAdd(@PathVariable Integer invoiceId){
        ApiResponse apiResponse=paymentService.paymentAdd(invoiceId);
        return ResponseEntity.status(apiResponse.isSuccess()?200:409).body(apiResponse);
    }


    //This method obtains payment details via payment_ id
    @GetMapping("/payment/details")
    public HttpEntity<?> getPaymentDetails(@RequestParam Integer paymentId){
        ApiResponse apiResponse=paymentService.getPaymentDetails(paymentId);
       return ResponseEntity.status(apiResponse.isSuccess()?200:404).body(apiResponse);
    }

    //This method get to overpayment and invoiceId
    @GetMapping("/getInvoiceIdAndOverPayment")
    public HttpEntity<?> getInvoiceIdAndOverPayment(){
        ApiResponse apiResponse=paymentService.getInvoiceIdAndOverPayment();
            return ResponseEntity.ok(apiResponse);
        }
    }

