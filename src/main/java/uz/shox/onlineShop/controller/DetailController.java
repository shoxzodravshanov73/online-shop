package uz.shox.onlineShop.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.shox.onlineShop.payload.ApiResponse;
import uz.shox.onlineShop.service.DetailService;

@RestController
@RequestMapping("/api")
public class DetailController {

    private final DetailService detailService;

    public DetailController(DetailService detailService) {
        this.detailService = detailService;
    }

    /*This method takes the id
     and date of products ordered more than 10 times in total
     */
    @GetMapping("/getProductIdAndCountOrder")
    public HttpEntity<?> getProductIdAndCountOrder(){
        ApiResponse apiResponse=detailService.getProductIdAndCountOrder();
        return ResponseEntity.ok(apiResponse);
    }
}
