package uz.shox.onlineShop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.shox.onlineShop.entity.Category;
import uz.shox.onlineShop.payload.ApiResponse;
import uz.shox.onlineShop.service.CategoryService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class CategoryController {


    private final CategoryService categoryService;


    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }




    //This method get all categories list

    @GetMapping("/category/list")
    public HttpEntity<?> allCategory(){
       ApiResponse apiResponse= categoryService.allCategory();
        return ResponseEntity.ok(apiResponse);
    }

    //this method get a category via product_id
    @GetMapping("/category")
    public HttpEntity<?> oneCategory(@RequestParam Integer product_Id){
       ApiResponse apiResponse= categoryService.oneCategory(product_Id);
       return ResponseEntity.status(apiResponse.isSuccess()?200:404).body(apiResponse);
    }


}
