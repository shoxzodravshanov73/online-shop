package uz.shox.onlineShop.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.shox.onlineShop.payload.ApiResponse;
import uz.shox.onlineShop.service.CategoryService;
import uz.shox.onlineShop.service.ProductService;

@RestController
@RequestMapping("/api")
public class ProductController {


    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    //This method get all products list

    @GetMapping("/product/list")
    public HttpEntity<?> allProduct(){
       ApiResponse apiResponse= productService.allProduct();
        return ResponseEntity.ok(apiResponse);
    }

    //this method retrieves product details via product_id
    @GetMapping("/product")
    public HttpEntity<?> oneProduct(@RequestParam Integer product_Id){
       ApiResponse apiResponse= productService.oneProduct(product_Id);
       return ResponseEntity.status(apiResponse.isSuccess()?200:404).body(apiResponse);
    }

    //This method get all bulk products
    @GetMapping("getBulkProducts")
    public HttpEntity<?> getBulkProducts(){
        ApiResponse apiResponse=productService.getBulkProducts();
        return ResponseEntity.ok(apiResponse);
    }
}
