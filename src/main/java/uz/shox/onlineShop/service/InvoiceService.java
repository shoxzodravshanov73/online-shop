package uz.shox.onlineShop.service;

import org.springframework.stereotype.Service;
import uz.shox.onlineShop.entity.Invoice;
import uz.shox.onlineShop.objectDto.InvoiceObjectDto;
import uz.shox.onlineShop.payload.ApiResponse;
import uz.shox.onlineShop.repository.InvoiceRepository;

import java.sql.Date;
import java.util.List;

@Service
public class InvoiceService {

    private final InvoiceRepository invoiceRepository;

    public InvoiceService(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }


    public ApiResponse getAfterDateDueInvoice() {
            List<Invoice> invoiceList = invoiceRepository.getAfterDateIssuedInvoice();
        return new ApiResponse("Successfully", true,invoiceList);
    }

    public ApiResponse getBeforeDateOrderInvoice() {
        List<InvoiceObjectDto> invoiceList = invoiceRepository.getBeforeDateOrderInvoice();
        return new ApiResponse("Successfully", true,invoiceList);
    }
}
