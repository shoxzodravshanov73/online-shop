package uz.shox.onlineShop.service;

import org.springframework.stereotype.Service;
import uz.shox.onlineShop.entity.*;
import uz.shox.onlineShop.objectDto.NumberOfProductsInYearObjectDto;
import uz.shox.onlineShop.objectDto.OrdersWithoutInvoicesObjectDto;
import uz.shox.onlineShop.payload.ApiResponse;
import uz.shox.onlineShop.payload.OrderDto;
import uz.shox.onlineShop.repository.*;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Service
public class OrderService {

    private final OrderRepository orderRepository;

    private final InvoiceRepository invoiceRepository;

    private final CustomerRepository customerRepository;

    private final DetailService detailService;

    private final DetailRepository detailRepository;
    private final ProductRepository productRepository;

    public OrderService(OrderRepository orderRepository, InvoiceRepository invoiceRepository, CustomerRepository customerRepository, DetailService detailService, DetailRepository detailRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.invoiceRepository = invoiceRepository;
        this.customerRepository = customerRepository;
        this.detailService = detailService;
        this.detailRepository = detailRepository;
        this.productRepository = productRepository;
    }

    public ApiResponse addOrder(OrderDto orderDto) {

        Optional<Customer> optional = customerRepository.findById(orderDto.getCustomerId());
        if (optional.isPresent()){
            Customer customer = optional.get();
            Order order=new Order();
            order.setCustomer(customer);
            order.setDate(new Date(Calendar.getInstance().getTime().getTime()));
            Order saveOrder = orderRepository.save(order);
            Double amount = detailService.getAmount(saveOrder, orderDto);
            Invoice invoice=new Invoice();
            invoice.setAmount(amount);
            invoice.setOrder(order);
            Invoice saveInvoice = invoiceRepository.save(invoice);


            return new ApiResponse("Successfully",true,saveInvoice.getId());
        }
        return new ApiResponse("Error",false);
    }


    public ApiResponse getOrder(Integer orderId) {
        Optional<Order> orderOptional = orderRepository.findById(orderId);
        if (orderOptional.isPresent()){
            Order order = orderOptional.get();
            Detail detail = detailRepository.findByOrderId(order.getId());
            Optional<Product> productOptional = productRepository.findById(detail.getProduct().getId());
            if (productOptional.isPresent()){
                Product product = productOptional.get();
                return new ApiResponse("This product name : "+product.getName(),true,order);
            }
        }

        return new ApiResponse("Error",false);
    }

    public ApiResponse getOrders() {

        List<Order> byDateBeforeOrders = orderRepository.getByDateBeforeOrders();
        if (byDateBeforeOrders==null){
            return new ApiResponse("There are no orders before this date",false,byDateBeforeOrders);
        }

        return new ApiResponse("Successfully",true,byDateBeforeOrders);
    }

    public ApiResponse numberOfProductsInYear() {
        List<NumberOfProductsInYearObjectDto> numberOrder = orderRepository.getNumberOrder();
        return new ApiResponse("Successfully",true,numberOrder);
    }

    public ApiResponse getOrdersWithoutInvoices() {
        List<OrdersWithoutInvoicesObjectDto> ordersWithoutInvoices = orderRepository.getOrdersWithoutInvoices();
        return new ApiResponse("Successfully",true,ordersWithoutInvoices);
    }
}
