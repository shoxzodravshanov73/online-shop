package uz.shox.onlineShop.service;

import org.springframework.stereotype.Service;
import uz.shox.onlineShop.entity.Category;
import uz.shox.onlineShop.entity.Product;
import uz.shox.onlineShop.objectDto.BulkProductsObjectDto;
import uz.shox.onlineShop.payload.ApiResponse;
import uz.shox.onlineShop.repository.ProductRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    public ApiResponse allProduct() {
        List<Product> productList = productRepository.findAll();
        return new ApiResponse("Successfully",true,productList);
    }

    public ApiResponse oneProduct(Integer product_id) {
        Optional<Product> productOptional = productRepository.findById(product_id);
        if (productOptional.isPresent()){
            Product product = productOptional.get();
            return new ApiResponse("Successfully",true,product);
        }
        return new ApiResponse("Not Found",false);
    }

    public ApiResponse getBulkProducts() {
        List<BulkProductsObjectDto> bulkProducts = productRepository.getBulkProducts();
        return new ApiResponse("Successfully",true,bulkProducts);
    }
}
