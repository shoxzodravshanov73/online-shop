package uz.shox.onlineShop.service;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.stereotype.Service;
import uz.shox.onlineShop.entity.Invoice;
import uz.shox.onlineShop.entity.Payment;
import uz.shox.onlineShop.objectDto.OverPaymentObjectDto;
import uz.shox.onlineShop.payload.ApiResponse;
import uz.shox.onlineShop.repository.InvoiceRepository;
import uz.shox.onlineShop.repository.PaymentRepository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Service
public class PaymentService {

    private final PaymentRepository paymentRepository;
    private final InvoiceRepository invoiceRepository;

    public PaymentService(PaymentRepository paymentRepository, InvoiceRepository invoiceRepository) {
        this.paymentRepository = paymentRepository;
        this.invoiceRepository = invoiceRepository;
    }

    @CreationTimestamp
    private Timestamp date;

    public ApiResponse paymentAdd(Integer invoiceId) {
        Optional<Invoice> invoiceOptional = invoiceRepository.findById(invoiceId);
        if (invoiceOptional.isPresent()){
            Invoice invoice = invoiceOptional.get();
            Payment payment=new Payment();
            payment.setInvoice(invoice);
            payment.setAmount(invoice.getAmount());
            payment.setTime(date);
            Payment save = paymentRepository.save(payment);
            return new ApiResponse("Successfully",true,save);
        }
      return new ApiResponse("Filed",false);

    }

    public ApiResponse getPaymentDetails(Integer paymentId) {
        Optional<Payment> paymentOptional = paymentRepository.findById(paymentId);
        if (paymentOptional.isPresent()){
            Payment payment = paymentOptional.get();
            return new ApiResponse("Successfully",true,payment);
        }
        return new ApiResponse("Not found",false);
    }

    public ApiResponse getInvoiceIdAndOverPayment() {
        List<OverPaymentObjectDto> invoiceIdAndOverPayment = paymentRepository.getInvoiceIdAndOverPayment();
        return new ApiResponse("Successfully",true,invoiceIdAndOverPayment);
    }
}
