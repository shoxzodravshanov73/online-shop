package uz.shox.onlineShop.service;

import org.springframework.stereotype.Service;
import uz.shox.onlineShop.entity.Detail;
import uz.shox.onlineShop.entity.Order;
import uz.shox.onlineShop.entity.Product;
import uz.shox.onlineShop.objectDto.ProductIdCountOrderObjectDto;
import uz.shox.onlineShop.payload.ApiResponse;
import uz.shox.onlineShop.payload.OrderDto;
import uz.shox.onlineShop.repository.DetailRepository;
import uz.shox.onlineShop.repository.ProductRepository;

import java.util.List;

@Service
public class DetailService {


    private final DetailRepository detailRepository;


    private final ProductRepository productRepository;

    public DetailService(DetailRepository detailRepository, ProductRepository productRepository) {
        this.detailRepository = detailRepository;
        this.productRepository = productRepository;
    }
//This method calculates the amount of invoice
    public Double getAmount(Order order, OrderDto dto){
        Product product = productRepository.getById(dto.getProductId());
        Detail detail=new Detail();
        detail.setProduct(product);
        detail.setQuantity(dto.getQuantity());
        detail.setOrder(order);
        detailRepository.save(detail);
        Double amount=product.getPrice() * dto.getQuantity();
        return  amount;
    }

    public ApiResponse getProductIdAndCountOrder() {
        List<ProductIdCountOrderObjectDto> productIdAndCountOrder = detailRepository.getProductIdAndCountOrder();
        return new ApiResponse("Successfully",true,productIdAndCountOrder);
    }
}
