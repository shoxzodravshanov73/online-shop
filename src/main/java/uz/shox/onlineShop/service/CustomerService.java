package uz.shox.onlineShop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.shox.onlineShop.entity.Customer;
import uz.shox.onlineShop.objectDto.CustomerLastOrderObjectDto;
import uz.shox.onlineShop.payload.ApiResponse;
import uz.shox.onlineShop.payload.CustomerDto;
import uz.shox.onlineShop.repository.CustomerRepository;


import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }




    public ApiResponse getCustomersAllDetails() {
        List<Customer> customers = customerRepository.getCustomers();
        if (customers==null){
            return new ApiResponse("There are no customers who have not placed an order on a given date",false,customers);
        }
        return new ApiResponse("Successfully",true,customers);
    }


    public ApiResponse getCustomerLastOrderDate() {
        List<CustomerLastOrderObjectDto> customerByLastOrderDate = customerRepository.getCustomerByLastOrderDate();
        return new ApiResponse("Successfully",true,customerByLastOrderDate);
    }
}
