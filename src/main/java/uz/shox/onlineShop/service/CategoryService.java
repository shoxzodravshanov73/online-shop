package uz.shox.onlineShop.service;


import org.springframework.stereotype.Service;
import uz.shox.onlineShop.entity.Category;
import uz.shox.onlineShop.entity.Product;
import uz.shox.onlineShop.payload.ApiResponse;
import uz.shox.onlineShop.repository.CategoryRepository;
import uz.shox.onlineShop.repository.ProductRepository;

import java.util.List;
import java.util.Optional;
@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final ProductRepository productRepository;



    public CategoryService(CategoryRepository categoryRepository, ProductRepository productRepository) {
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
    }



    public ApiResponse allCategory() {
        List<Category> categoryList = categoryRepository.findAll();
        return new ApiResponse("Successfully",true,categoryList);
    }

    public ApiResponse oneCategory(Integer product_id) {

        Optional<Product> productOptional = productRepository.findById(product_id);
        if (productOptional.isPresent()){
            Product product = productOptional.get();
            Category category = product.getCategory();
            return new ApiResponse("Successfully",true,category);
        }
        return new ApiResponse("Not Found",false);
    }

}
