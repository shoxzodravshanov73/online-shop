package uz.shox.onlineShop.objectDto;

import java.util.Date;

public interface InvoiceObjectDto {

    Integer getid();

    Integer getorder_id();

    Date getissued();

    Date getdate();

}
