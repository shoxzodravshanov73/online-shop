package uz.shox.onlineShop.objectDto;

import java.sql.Date;

public interface CustomerLastOrderObjectDto {
    Integer getid();

    String getname();

    Date  getlastOrderDate();
}
