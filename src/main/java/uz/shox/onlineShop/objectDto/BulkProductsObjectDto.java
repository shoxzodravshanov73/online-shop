package uz.shox.onlineShop.objectDto;

public interface BulkProductsObjectDto {
    Integer getid();

    Double getprice();
}
