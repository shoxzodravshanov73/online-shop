package uz.shox.onlineShop.objectDto;

public interface ProductIdCountOrderObjectDto {

    Integer getproductId();

    Integer getcountOrder();
}
