package uz.shox.onlineShop.objectDto;

public interface OverPaymentObjectDto {

    Integer getinvoiceId();

    Double getoverpayment();
}
