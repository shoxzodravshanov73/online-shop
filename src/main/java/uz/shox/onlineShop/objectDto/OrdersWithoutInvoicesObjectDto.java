package uz.shox.onlineShop.objectDto;

import java.sql.Date;

public interface OrdersWithoutInvoicesObjectDto {
    Integer getorderId();

    Date getdate();

    Double gettotalSum();
}
