package uz.shox.onlineShop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.sql.Date;

@SpringBootApplication
public class OnlineShopApplication  {


    public static void main(String[] args) {

        SpringApplication.run(OnlineShopApplication.class, args);
    }




}
