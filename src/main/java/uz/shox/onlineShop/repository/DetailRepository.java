package uz.shox.onlineShop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.shox.onlineShop.entity.Detail;
import uz.shox.onlineShop.entity.Order;
import uz.shox.onlineShop.entity.Product;
import uz.shox.onlineShop.objectDto.ProductIdCountOrderObjectDto;
import uz.shox.onlineShop.payload.OrderDto;

import java.util.List;

public interface DetailRepository extends JpaRepository<Detail,Integer> {

    Detail findByOrderId(Integer ord_id);

    @Query(value = "select product_id as productId,sum(quantity) as countOrder\n" +
            "from detail\n" +
            "group by product_id having sum(quantity)>10\n" +
            "order by product_id",nativeQuery = true)//7
    List<ProductIdCountOrderObjectDto> getProductIdAndCountOrder();

}
