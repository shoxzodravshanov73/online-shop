package uz.shox.onlineShop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.shox.onlineShop.entity.Category;
import uz.shox.onlineShop.entity.Product;
import uz.shox.onlineShop.objectDto.BulkProductsObjectDto;

import java.sql.Date;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product,Integer> {

    @Query(value = "select p.id, p.price from product p\n" +
            "join detail d on p.id = d.product_id\n" +
            "where d.quantity>=8\n" +
            "group by p.id\n" +
            "order by p.id",nativeQuery = true)

    List<BulkProductsObjectDto> getBulkProducts();



}
