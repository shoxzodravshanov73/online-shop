package uz.shox.onlineShop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.shox.onlineShop.entity.Invoice;
import uz.shox.onlineShop.objectDto.InvoiceObjectDto;

import java.sql.Date;
import java.util.List;

public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {

    @Query(value = "select i.* from invoice i where i.due<now()",nativeQuery = true)//1
    List<Invoice> getAfterDateIssuedInvoice();


    @Query(value ="select i.id, i.issued, i.order_id, my.date\n" +
            "from invoice i\n" +
            "         inner join my_order my on my.id = i.order_id\n" +
            "where i.issued<=my.date" , nativeQuery = true)//2
    List<InvoiceObjectDto> getBeforeDateOrderInvoice();
}
