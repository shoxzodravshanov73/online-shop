package uz.shox.onlineShop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.shox.onlineShop.entity.Payment;
import uz.shox.onlineShop.objectDto.OverPaymentObjectDto;

import java.util.List;

public interface PaymentRepository extends JpaRepository<Payment,Integer> {

    Payment findByInvoiceId(Integer inv_id);

    @Query(value = "select i.id as invoiceId, sum(p.amount)-i.amount as overpayment\n" +
            "from invoice i\n" +
            "join payment p on i.id = p.invoice_id\n" +
            "group by i.id,p.invoice_id, i.amount\n" +
            "having sum(p.amount)>i.amount and count(p.invoice_id)>1\n" +
            "order by p.invoice_id", nativeQuery = true)
    List<OverPaymentObjectDto> getInvoiceIdAndOverPayment();
}
