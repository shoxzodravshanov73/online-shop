package uz.shox.onlineShop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.shox.onlineShop.entity.Category;
@Repository
public interface CategoryRepository extends JpaRepository<Category,Integer> {

}
