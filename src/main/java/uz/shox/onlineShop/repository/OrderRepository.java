package uz.shox.onlineShop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.shox.onlineShop.entity.Order;
import uz.shox.onlineShop.objectDto.NumberOfProductsInYearObjectDto;
import uz.shox.onlineShop.objectDto.OrdersWithoutInvoicesObjectDto;

import java.sql.Date;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order,Integer> {

    @Query(value = "select * from my_order\n" +
            "where date <'2016-09-06'",nativeQuery = true)//3
    List<Order> getByDateBeforeOrders();


    @Query(value = "select count(o.id) countOrder from my_order o\n" +
            "where o.date between '2016-01-01' and '2016-12-31'",nativeQuery = true)//9

    List<NumberOfProductsInYearObjectDto> getNumberOrder();



    @Query(value = "select mo.id as orderId,mo.date as date, product.price*d.quantity as totalSum from product\n" +
            "join detail d on product.id = d.product_id\n" +
            " join my_order mo on mo.id = d.order_id\n" +
            "left join invoice i on mo.id = i.order_id\n" +
            "where i.order_id is null",nativeQuery = true)
    List<OrdersWithoutInvoicesObjectDto> getOrdersWithoutInvoices();

}
