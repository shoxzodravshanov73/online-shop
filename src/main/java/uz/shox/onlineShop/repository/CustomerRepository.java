package uz.shox.onlineShop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.shox.onlineShop.entity.Customer;
import uz.shox.onlineShop.objectDto.CustomerLastOrderObjectDto;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    @Query(value = "select c.* from customer c\n" +
            "join my_order mo on c.id = mo.customer_id\n" +
            "where mo.date not between '2016-01-01' and '2016-12-31'",nativeQuery = true)//4

    List<Customer> getCustomers();




    @Query(value = "select c.id as id, c.name as name,  max(mo.date) as lastOrderDate from customer c\n" +
            "join my_order mo on c.id = mo.customer_id\n" +
            "group by c.name, c.id having count(mo.customer_id)>1\n" +
            "order by c.id",nativeQuery = true)//5

    List<CustomerLastOrderObjectDto> getCustomerByLastOrderDate();



}
