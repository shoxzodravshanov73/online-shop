package uz.shox.onlineShop.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.shox.onlineShop.entity.Order;
import uz.shox.onlineShop.entity.Product;

import javax.persistence.Column;
import javax.persistence.OneToOne;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DetailDto {

    private Integer productId;

    private Integer orderId;

    private Integer quantity;
}
