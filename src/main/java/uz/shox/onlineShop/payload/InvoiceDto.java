package uz.shox.onlineShop.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.shox.onlineShop.entity.Order;

import javax.persistence.Column;
import javax.persistence.OneToOne;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceDto {


    private Integer orderId;

    private double amount;

    private Date isSued;

    private Date due;


}
