package uz.shox.onlineShop.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class CustomerDto {

    @NotNull(message = "Name must be")
    private String name;

    @NotNull(message = "Country must be")
    private String country;

    private String address;


    private String phone;
}
