package uz.shox.onlineShop.payload;

import lombok.Data;

@Data
public class OrderDto {

    private Integer customerId;
    private Integer productId;
    private Short quantity;
}
