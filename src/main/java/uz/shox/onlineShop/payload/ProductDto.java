package uz.shox.onlineShop.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    @NotNull
    private String name;

    private Integer categoryId;

    private String description;

    private double price;

    private Integer photoId;
}
