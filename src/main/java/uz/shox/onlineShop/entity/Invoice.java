package uz.shox.onlineShop.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.shox.onlineShop.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import java.sql.Date;
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Invoice extends AbsEntity {

    @OneToOne
    private Order order;

    @Column(nullable = false)
    private Double amount;

    @Column(nullable = false)
    private Date issued;

    @Column(nullable = false)
    private Date due;

}
